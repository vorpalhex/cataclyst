## Basic Details

+ OS: _Windows/MacOS/Linux_
+ Version: _10/11.2_

## Log file

_Upload the contents of your cataclyst.log file to https://pastebin.com/ and replace this block with a link to your paste_

## What Happened?

_Describe what happened. What were you doing when the problem occurred? What did you expect to happen?_