module gitlab.com/vorpalhex/cataclyst

go 1.15

require (
	fyne.io/fyne/v2 v2.0.3
	github.com/google/go-github/v32 v32.1.0
	github.com/otiai10/copy v1.6.0
	github.com/rs/zerolog v1.23.0
)
