package main

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	"github.com/google/go-github/v32/github"
)

var versionSelect *widget.Select
var gameVersions []string
var topBarLayout fyne.Container
var selectedTag string
var selectedGame *Game
var selectedReleases []*github.RepositoryRelease
var actionBtn *widget.Button
var gameDescription *widget.Label
var releaseNotes *widget.Label

func StartUI() {
	app := app.New()
	window := app.NewWindow("Cataclyst Launcher")

	gameNames := make([]string, len(GAMES))
	for i, game := range GAMES {
		gameNames[i] = game.name
	}

	//widgets
	gameSelect := widget.NewSelect(gameNames, SelectGame)
	versionSelect = widget.NewSelect(gameVersions, SelectVersion)
	gameDescription = widget.NewLabel("")
	gameDescription.Wrapping = fyne.TextWrapWord
	gameDescription.Alignment = fyne.TextAlignLeading
	releaseNotes = widget.NewLabel("")
	releaseNotes.Wrapping = fyne.TextWrapWord
	releaseNotes.Alignment = fyne.TextAlignLeading
	actionBtn = widget.NewButton("...checking", ClickAction)
	actionBtn.Importance = widget.HighImportance
	gameSelect.SetSelectedIndex(0)

	//wrappers

	//containers
	window.Resize(fyne.Size{Width: 600, Height: 400})
	topBarLayout := container.New(
		layout.NewHBoxLayout(),
		container.New(
			layout.NewHBoxLayout(),
			container.NewVBox(widget.NewLabel("Choose your Game"), gameSelect),
			container.NewVBox(widget.NewLabel("Select Game Version                      \t"), versionSelect),
		),
		layout.NewSpacer(),
		container.NewVBox(widget.NewLabel("               \t"), actionBtn))

	borderLayout := container.NewBorder(topBarLayout, nil, nil, nil, container.NewVBox(gameDescription, layout.NewSpacer(), releaseNotes))

	window.SetContent(borderLayout)
	window.ShowAndRun()
}

func SelectGame(selected string) {
	selectedGame = GetGameByName(selected)
	gameDescription.SetText(selectedGame.description)
	_, selectedReleases = GetReleases(selectedGame)

	releaseTags := make([]string, len(selectedReleases))
	for i, release := range selectedReleases {
		releaseTags[i] = release.GetTagName()
	}

	versionSelect.Options = releaseTags
	versionSelect.SetSelectedIndex(0)
	versionSelect.Refresh()
}

func SelectVersion(selected string) {
	selectedTag = selected
	release, releaseErr := TagToRelease(selectedTag, selectedReleases)
	CheckFatal(releaseErr, "Failed to find valid release")

	releaseNotes.SetText(release.GetBody())

	if HaveGameData(selectedTag, selectedGame) == true {
		actionBtn.Text = "Play"
	} else {
		actionBtn.Text = "Download"
	}
	actionBtn.Refresh()
}

func ClickAction() {
	if HaveGameData(selectedTag, selectedGame) == true {
		LaunchGame(selectedTag, selectedGame)
	} else {
		actionBtn.Disable()
		actionBtn.Text = "Downloading..."
		actionBtn.Refresh()
		DownloadAndInstall(selectedTag, selectedGame)
		actionBtn.Text = "Play"
		actionBtn.Enable()
		actionBtn.Refresh()
	}
}
