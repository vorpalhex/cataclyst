package main

type Game struct {
	name        string
	shortname   string
	org         string
	repo        string
	description string
	dmgPath     string
	appPath     string
}

var GAMES []Game

func LoadConstants() {
	GAMES = []Game{
		{
			name:      "Bright Nights",
			shortname: "bn",
			repo:      "Cataclysm-BN",
			org:       "cataclysmbnteam",
			description: `Cataclysm: Bright Nights
A fork/variant of Cataclysm:DDA by CleverRaven.

Bright Nights features buildings with working electrical grids, significant game rebalances making more playstyles viable, and significant amounts of restored content that was removed in DDA.
			
BN is focused on a more casual play style in general. If you're new to Cataclysm, Bright Nights is the perfect place to start.`,
			dmgPath: "/Volumes/Cataclysm BN",
			appPath: "/Volumes/Cataclysm BN/Cataclysm.app",
		},
	}
}
