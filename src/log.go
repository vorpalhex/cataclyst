package main

import (
	"os"
	"path"
	"runtime"

	"github.com/rs/zerolog"
	eLog "github.com/rs/zerolog/log"
	"github.com/rs/zerolog/pkgerrors"
)

var log zerolog.Logger

func SetupLog() {
	//create your file with desired read/write permissions
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.ErrorStackMarshaler = pkgerrors.MarshalStack

	logPath := path.Join(GetLogPath(), "/cataclyst.log")

	f, err := os.OpenFile(logPath, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		//uhhh shit, we kinda have a fatal error but no logger yet
		log = eLog.Logger
		Log("Error trying to get a logger", err)
	} else {
		log = zerolog.New(f).With().Logger()
	}

	Log("Program launch")
	Log("Operating System: %v\n", runtime.GOOS)
	Log("Arch: %v\n", runtime.GOARCH)
}

func CheckFatal(pErr error, context string) {
	if pErr != nil {
		log.Fatal().Stack().Err(pErr).Msg(context)
		os.Exit(1)
	}
}

func Log(format string, args ...interface{}) {
	log.Printf(format, args...)
}
