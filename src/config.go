package main

import (
	"os"
	"path"
	"strings"
)

//this helps us escape .app folders for MacOS
func EscapeAppDir(oldPath string) string {
	cleanedPath := oldPath

	depth := 0
	for strings.Contains(cleanedPath, ".app") {
		cleanedPath = path.Dir(cleanedPath)
		depth++
		if depth > 6 { //if we're more than six levels deep something is wrong
			break
		}
	}

	return cleanedPath
}

func GetInstallPath(game *Game, tag string) string {
	cwd, _ := os.Getwd()
	//handle macos .app structure
	cwd = EscapeAppDir(cwd)
	return path.Join(cwd, "installs", game.shortname, tag)
}

func GetLogPath() string {
	cwd, _ := os.Getwd()
	//handle macos .app structure
	cwd = EscapeAppDir(cwd)
	return cwd
}

func GetTempPath() string {
	firstTemp := os.TempDir()
	cwd, _ := os.Getwd()
	cwd = EscapeAppDir(cwd)
	secondTemp := path.Join(cwd, "tmp")

	_, err := os.Stat(firstTemp)

	if err != nil {
		return secondTemp
	}

	return firstTemp
}
