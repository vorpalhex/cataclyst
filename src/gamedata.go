package main

import (
	"io"
	"os"
	"os/exec"
	"path"
	"runtime"
	"strings"

	rCopy "github.com/otiai10/copy"
)

func HaveGameData(releaseName string, selectedGame *Game) bool {
	installPath := GetInstallPath(selectedGame, selectedTag)

	gameName := GetApplicationName()
	gamePath := path.Join(installPath, gameName)
	_, gamePathErr := os.Stat(gamePath)
	if gamePathErr != nil {
		return false
	}

	return true
}

func DownloadAndInstall(selectedTag string, selectedGame *Game) {
	asset, assetErr := TagToAsset(selectedTag, selectedReleases)

	CheckFatal(assetErr, "Failed to convert tag to asset")

	assetName := asset.GetName()
	tempPath := GetTempPath()
	installPath := GetInstallPath(selectedGame, selectedTag)
	Log("Downloading %v\n", assetName)
	//make sure our temp path is good
	os.MkdirAll(tempPath, os.ModePerm)

	//make sure our install path is good
	os.MkdirAll(installPath, os.ModePerm)

	httpClient := GetHttpClient()

	resp, err := httpClient.Get(asset.GetBrowserDownloadURL())
	contentType := asset.GetContentType()

	CheckFatal(err, "Download error")

	defer resp.Body.Close()
	Log("download content type: %v\n", contentType)

	switch contentType {
	case "application/gzip":
		target := path.Join(tempPath, assetName)
		file, err := os.Create(target)
		CheckFatal(err, "error creating download target")

		_, copyErr := io.Copy(file, resp.Body)
		CheckFatal(copyErr, "error copying to download target")

		subTargetPath := strings.Replace(target, ".tar.gz", ".tar", 1)

		gzipErr := UnGzip(target, subTargetPath)
		CheckFatal(gzipErr, "error ungzipping")

		untarErr := Untar(subTargetPath, installPath)
		CheckFatal(untarErr, "error during untar")

		break
	case "application/zip":
		target := path.Join(tempPath, assetName)
		file, err := os.Create(target)
		CheckFatal(err, "error creating download target")

		_, copyErr := io.Copy(file, resp.Body)
		CheckFatal(copyErr, "error copying to download target")

		unzipErr := Unzip(target, installPath)
		CheckFatal(unzipErr, "error unzipping")

		break
	case "application/x-apple-diskimage":
		target := path.Join(tempPath, assetName)
		file, err := os.Create(target)
		CheckFatal(err, "error creating download target")

		_, copyErr := io.Copy(file, resp.Body)
		CheckFatal(copyErr, "error copying download to target")

		attachCmd := exec.Command("hdiutil", "attach", target)
		attachErr := attachCmd.Run()
		CheckFatal(attachErr, "error attaching dmg")

		appPath := path.Join(installPath, "Cataclysm.app")
		appCopyErr := rCopy.Copy(selectedGame.appPath, appPath)
		CheckFatal(appCopyErr, "app copy from dmg err")

		detachCmd := exec.Command("hdiutil", "detach", selectedGame.dmgPath)
		detachErr := detachCmd.Run()
		CheckFatal(detachErr, "error detaching dmg")

		break
	default:
		Log("Unsupported ContentType: %v", contentType)
	}
}

func LaunchGame(tag string, selectedGame *Game) {
	osName := runtime.GOOS
	appName := GetApplicationName()
	installPath := GetInstallPath(selectedGame, tag)
	appPath := path.Join(installPath, appName)

	Log("Launching %v\n", appPath)

	var runCmd *exec.Cmd
	switch osName {
	case "windows":
		runCmd = exec.Command(appPath)
	case "darwin":
		runCmd = exec.Command("open", appPath)
	case "linux":
		runCmd = exec.Command(appPath)
	}

	runCmd.Dir = installPath
	runErr := runCmd.Start()
	CheckFatal(runErr, "error launching application")
}
