package main

import (
	"context"
	"errors"
	"regexp"
	"runtime"

	"github.com/google/go-github/v32/github"
)

var ctx = context.Background()

func GetReleases(game *Game) (error, []*github.RepositoryRelease) {
	client := github.NewClient(nil)
	releases, _, err := client.Repositories.ListReleases(ctx, game.org, game.repo, &github.ListOptions{Page: 0, PerPage: 100})

	if err != nil {
		return err, nil
	}

	return err, releases
}

func TagToAsset(tag string, releases []*github.RepositoryRelease) (*github.ReleaseAsset, error) {
	os := runtime.GOOS
	arch := runtime.GOARCH

	osNeedle := "unknown"
	archNeedle := "unknown"

	switch os {
	case "windows":
		osNeedle = "windows"
	case "darwin":
		osNeedle = "osx"
	case "linux":
		osNeedle = "linux"
	}

	switch arch {
	case "386":
		archNeedle = "x32"
	case "amd64":
		archNeedle = "x64"
	}

	needle := osNeedle + "-tiles-" + archNeedle

	var selectedRelease *github.RepositoryRelease

	for _, release := range releases {
		if tag == release.GetTagName() {
			selectedRelease = release
		}
	}

	for _, asset := range selectedRelease.Assets {
		matched, _ := regexp.MatchString(needle, asset.GetName())
		//fmt.Printf("needle %s haystack %s", needle, asset.GetName())
		if matched {
			return asset, nil
		}
	}

	return nil, errors.New("No assets matched this platform!")
}

func TagToRelease(tag string, releases []*github.RepositoryRelease) (*github.RepositoryRelease, error) {
	for _, release := range releases {
		if tag == release.GetTagName() {
			return release, nil
		}
	}

	return nil, errors.New("No releases matched")
}
