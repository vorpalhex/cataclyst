# Cataclyst

**Currently in ALPHA**

A launcher for Cataclysm Games

Cataclysm is a cross-platform golang based launcher for Cataclysm games, with support initially focusing on [Cataclysm: Bright Nights](https://github.com/cataclysmbnteam/Cataclysm-BN). Builds are currently available for Windows and MacOS, with Linux builds coming soon. 

We currently support:

+ Viewing changelogs
+ Selecting version
+ Downloading and unpacking
+ Launching

Near future features will include:

+ Migrating save files between versions
+ Backing up saves before launch
+ Migrating other folders such as keymappings
+ Mod management

## Usage

Extract the zip file, drop the executable where you desire to use it. Currently your game data will be installed alongside the launcher, as will the log file.

## Reporting Issues

Please open an issue and include your log file. 

## FAQ

+ Why not PrimativeLauncher/CDDA-Game-Launcher?
  
    My motivation is mainly related to wanting a cross-platform launcher that supported both BN and DDA.
+ Why no Linux support yet?
  
    I don't have a way to actually test them currently. Hardware will be ordered in a week or two. You are welcome to build yourself using CGO.

+ Why does this not have feature X/Y/Z?
  
    Time.

+ Why did you not use Rust/React/Whatever?

    Golang is easy, it had robust libraries for what we needed and I enjoy writing in it. You're welcome to write your launcher.

+ Will there be mobile builds?

    While our GUI framework supports it, mobile apps need very data handling and typically you're not allowed to dynamically download and run other programs. While we don't intend to release on them, others are welcome to fork this project. 

+ This doesn't work on my very old computer!

    Unfortuntely, no, it probably does not. Fyne is not the most lightweight UI option, but it offers a very nice UI and does so across almost all platforms. Unfortunately it needs a fair bit of horsepower.

## License

Copyright (C) 2020 VorpalHex

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.