start:
	cd src && go run *.go
.PHONY: start

clean:
	- rm -rf build
	- rm -rf src/cataclyst.app
	- rm -rf src/cataclyst.exe
	- rm -rf src/cataclyst.tar.gz

.PHONY: clean

build: clean
	mkdir -p build/windows
	mkdir -p build/linux
	mkdir -p build/macos
	cd src && fyne package -os darwin -icon ../resources/icon.png && mv cataclyst.app ../build/macos
	cd src && CGO_ENABLED=1 CC=x86_64-w64-mingw32-gcc GOOS=windows GOARCH=amd64 go build -ldflags -H=windowsgui -o ../build/windows/cataclyst-amd64.exe *.go
	# fyne package -icon resources/icon.png -executable build/windows/cataclsyt-amd64.exe
	# cd src && fyne package -os linux -icon ../resources/icon.png && mv cataclyst.tar.gz ../build/linux

	# cd src && CGO_ENABLED=1 CC=x86_64-w64-mingw32-gcc GOOS=windows GOARCH=amd64 go build -ldflags -H=windowsgui -o ../build/windows/cataclyst-amd64.exe *.go
	# cd src && GOOS=darwin GOARCH=amd64 go build -o ../build/macos/cataclyst *.go
	# cd src && GOOS=linux GOARCH=amd64 go build -ldflags -H=windowsgui -o ../build/windows/cataclyst-amd64 *.go
	# fyne package --icon resources/icon.png -executable build/macos/cataclyst
.PHONY: build

crosscompile-install:
	brew install mingw-w64
.PHONY: crosscompile-install